﻿import { Component, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { Services } from './services/services'

@Component({
    selector: 'my-list',
    template: `
    <ul *ngIf="data != null" style="margin-left:-30px;">
        <li *ngFor="let key of objectKeys(data)" class="list" id="listFormat">
            <a *ngIf="key != null && data?.constructor.name != 'Array'" id="a_padding">{{ key }} : <a *ngIf="data[key] != null && data[key]?.constructor.name != 'Object' && data[key].constructor.name != 'Array'" style="text-decoration: none;">{{data[key]}}</a></a>
            <a *ngIf="key != null && data?.constructor.name == 'Array'" id="a_padding">items :  <a *ngIf="data[key] != null && data[key]?.constructor.name != 'Object' && data[key].constructor.name != 'Array'" style="text-decoration: none;">{{data[key]}}</a></a>
            <input *ngIf="key != null && data[key]?.constructor.name == 'Object'" type="checkbox">
            <ul *ngIf="key != null && data[key] != null && data[key]?.constructor.name == 'Object'">
                <li *ngFor="let k of objectKeys(data[key])" class="list" id="listFormat">
                    <a *ngIf="k != null" id="a_padding">{{k}} : <a *ngIf="k != null && data[key][k] != null && data[key][k]?.constructor.name != 'Object' && data[key][k].constructor.name != 'Array'" style="text-decoration: none;">{{data[key][k]}}</a></a>
                    <my-list *ngIf="k != null && data[key][k] != null && data[key][k]?.constructor.name == 'Array'" [data]="data[key][k]"></my-list>
                </li>
            </ul>
            <my-list *ngIf="key != null && data[key] != null && data[key]?.constructor.name == 'Array'" [data]="data[key]"></my-list>
        </li>
    </ul>
`
})
//<ul>
//    <li * ngFor="let item of data" class="list" id = "listFormat" >
//    <a>item < /a><input type="checkbox">
//        < /li>
//        < /ul>
export class ArrayComponent {
    @Input() data: object;
    //obj: Object = JSON.parse(this.data);
    objectKeys = Object.keys;
    constructor() {
    }
}