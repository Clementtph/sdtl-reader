﻿export interface Expression {
    function: string;
    proprietaryOperator: string;
    arguments: Expression[];
    type: string;
    variableName: string;
}