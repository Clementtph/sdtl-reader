﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using C2Metadata.Web.Models;
using C2Metadata.Common.SpssConverter;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using C2Metadata.Common.Utility;

namespace C2Metadata.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(string data)
        {
            string info = data;
            //convert string to json and show in data
            var converter = new BasicConverter();
            var converted = converter.ConvertString(info);
            string json = SdtlSerializer.SerializeAsJson(converted);
            ViewData["data"] = "<h1>Result: </h1><pre>"+json+"</pre>";
            return View();
        }

        [HttpPost("api/spsstosdtl")]
        public async Task <IActionResult> spsstostdl()
        {
            string info = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                info = await reader.ReadToEndAsync();
            }
            //convert string to json and show in data
            var converter = new BasicConverter();
            var converted = converter.ConvertString(info);
            string json = SdtlSerializer.SerializeAsJson(converted);
            return Content(json, "application/json");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
