using C2Metadata.Common.SpssConverter;
using C2Metadata.Common.Utility;
using Microsoft.Extensions.CommandLineUtils;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Linq;

namespace C2Metadata.SpssToSdtl.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            var commandLineApplication = new CommandLineApplication(throwOnUnexpectedArg: false);

            var fileNameArgument = commandLineApplication.Argument("file", "The file to import", false);

            var verboseOption = commandLineApplication.Option("-v | --verbose", "Include detailed output", CommandOptionType.NoValue);
            var outputOption = commandLineApplication.Option("-o | --outputFile", "The output file to write", CommandOptionType.SingleValue);

            commandLineApplication.Name = "SPSSToSDTL";
            commandLineApplication.FullName = "SPSS to Standard Data Transform Language";
            commandLineApplication.Description = "SPSS to SDTL";
            commandLineApplication.ExtendedHelpText = string.Empty;

            commandLineApplication.HelpOption("-? | -h | --help");

            commandLineApplication.OnExecute(() =>
            {
                string fileName = fileNameArgument.Value;
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    bool isVerbose = verboseOption.HasValue();
                    string outputFile = outputOption.Value();

                    // Extract transforms.
                    var converter = new BasicConverter();
                    var program = converter.ConvertFile(fileName, isVerbose);

                    foreach (var message in program.Messages)
                    {
                        Console.WriteLine($"{message.LineNumber}:{message.CharacterPosition} - {message.Severity} - {message.MessageText}");
                    }

                    Console.WriteLine("Recognized command count: " + program.Commands.Count);

                    // Save the JSON file with transform descriptions.
                    if (!string.IsNullOrWhiteSpace(outputFile))
                    {
                        string json = SdtlSerializer.SerializeAsJson(program);
                        File.WriteAllText(outputFile, json);
                    }
                }

                return 0;
            });

            commandLineApplication.Execute(args);
        }
    }
}
