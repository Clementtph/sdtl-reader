﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sdtl
{

    public partial class AndCondition : FunctionCallExpression
    {
        public AndCondition() => Function = "and";

        public override string ToString()
        {
            return string.Join(" AND ", Arguments);
        }
    }

    public partial class OrCondition : FunctionCallExpression
    {
        public OrCondition() => Function = "or";

        public override string ToString()
        {
            return string.Join(" OR ", Arguments);
        }
    }

    public partial class NotCondition : FunctionCallExpression
    {
        public NotCondition() => Function = "not";

        public override string ToString()
        {
            return $"NOT {Arguments[0]}";
        }
    }

    public partial class EqualCondition : FunctionCallExpression
    {
        public EqualCondition() => Function = "eq";

        public override string ToString()
        {
            return $"{Arguments[0]} == {Arguments[1]}";
        }
    }

    public partial class NotEqualCondition : FunctionCallExpression
    {
        public NotEqualCondition() => Function = "ne";

        public override string ToString()
        {
            return $"{Arguments[0]} != {Arguments[1]}";
        }
    }

    public partial class GreaterThanCondition : FunctionCallExpression
    {
        public GreaterThanCondition() => Function = "gt";

        public override string ToString()
        {
            return $"{Arguments[0]} > {Arguments[1]}";
        }
    }

    public partial class GreaterThanOrEqualCondition : FunctionCallExpression
    {
        public GreaterThanOrEqualCondition() => Function = "gte";

        public override string ToString()
        {
            return $"{Arguments[0]} >= {Arguments[1]}";
        }
    }

    public partial class LessThanCondition : FunctionCallExpression
    {
        public LessThanCondition() => Function = "lt";

        public override string ToString()
        {
            return $"{Arguments[0]} < {Arguments[1]}";
        }
    }

    public partial class LessThanOrEqualCondition : FunctionCallExpression
    {
        public LessThanOrEqualCondition() => Function = "lte";

        public override string ToString()
        {
            return $"{Arguments[0]} <= {Arguments[1]}";
        }
    }

    public partial class GroupedCondition : FunctionCallExpression
    {
        public GroupedCondition() => Function = "parentheses";

        public override string ToString()
        {
            if (Arguments.Count == 0)
            {
                return "()";
            }

            return $"({Arguments[0]})";
        }
    }
}
