﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sdtl
{
    public partial class Program
    {
        public static int programCount = 1;

        public Program()
        {
            ID = "program-" + programCount++;
        }
    }

    public partial class SetDatasetProperty
    {
        public SetDatasetProperty()
        {
            Command = "setDatasetProperty";
        }
    }

    public partial class Invalid
    {
        public Invalid()
        {
            Command = "invalid";
        }
    }

    public partial class DoIf
    {
        public DoIf()
        {
            Command = "doif";
        }
    }

    public partial class Execute
    {
        public Execute()
        {
            Command = "execute";
        }
    }

    public partial class Rename
    {
        public Rename()
        {
            Command = "rename";
        }
    }

    public partial class ReshapeLong
    {
        public ReshapeLong()
        {
            Command = "reshapeLong";
        }
    }

    public partial class Select
    {
        public Select()
        {
            Command = "select";
        }
    }

    public partial class Compute
    {
        public Compute()
        {
            Command = "compute";
            Command = "load";

        }
    }

    public partial class Load
    {
        public Load()
        {
            Command = "load";
        }
    }

    public partial class Save
    {
        public Save()
        {
            Command = "save";
        }
    }

    public partial class ReshapeWide
    {
        public ReshapeWide()
        {
            Command = "reshapeWide";
        }
    }

    public partial class Recode
    {
        public Recode()
        {
            Command = "recode";
        }
    }

    public partial class RecodeRule
    {
        public override string ToString()
        {
            string from = string.Empty;
            if (!string.IsNullOrWhiteSpace(SpecialFromValue))
            {
                from = SpecialFromValue;
            }
            else if (FromValue.Count > 0)
            {
                from = string.Join(", ", FromValue);
            }
            else if (FromValueRange.Count > 0)
            {
                from = string.Join(", ", FromValueRange.Select(x => $"{x.First}..{x.Last}"));
            }

            string to = string.Empty;
            if (!string.IsNullOrWhiteSpace(SpecialToValue))
            {
                to = SpecialToValue;
            }
            else if (!string.IsNullOrWhiteSpace(To))
            {
                to = To;
            }

            if (string.IsNullOrWhiteSpace(Label))
            {
                return $"{from} -> {to}"; 
            }
            else
            {
                return $"{from} -> {to} ({Label})"; 
            }
        }
    }

    public partial class SetDisplayFormat
    {
        public SetDisplayFormat()
        {
            Command = "setDisplayFormat";
        }
    }

    public partial class MergeDatasets
    {
        public MergeDatasets()
        {
            Command = "mergeDatasets";
        }
    }

    public partial class SetMissingValues
    {
        public SetMissingValues()
        {
            Command = "setMissingValues";
        }
    }

    public partial class SetVariableLabel
    {
        public SetVariableLabel()
        {
            Command = "setVariableLabel";
        }
    }

    public partial class SetValueLabels
    {
        public SetValueLabels()
        {
            Command = "setValueLabels";
        }
    }

    public partial class Comment
    {
        public Comment()
        {
            Command = "comment";
        }
    }

    public partial class Delete
    {
        public Delete()
        {
            Command = "delete";
        }
    }

    public partial class KeepVariables
    {
        public KeepVariables()
        {
            Command = "keepVariables";
        }
    }

}
