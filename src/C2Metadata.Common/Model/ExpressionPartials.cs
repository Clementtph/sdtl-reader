﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sdtl
{
    public partial class StringConstantExpression
    {
        public StringConstantExpression()
        {
            Type = "string";
            TypeName = nameof(StringConstantExpression);
        }

        public override string ToString()
        {
            return Value;
        }
    }

    public partial class IntConstantExpression
    {
        public IntConstantExpression()
        {
            Type = "int";
            TypeName = nameof(IntConstantExpression);
        }

        public override string ToString()
        {
            return IntValue.ToString();
        }
    }

    public partial class DoubleConstantExpression
    {
        public DoubleConstantExpression()
        {
            Type = "double";
            TypeName = nameof(DoubleConstantExpression);
        }

        public override string ToString()
        {
            return DoubleValue.ToString();
        }
    }

    public partial class VariableSymbolExpression
    {
        public VariableSymbolExpression()
        {
            Type = "variable";
            TypeName = nameof(VariableSymbolExpression);
        }

        public override string ToString()
        {
            return VariableName;
        }
    }

    public partial class VariableRangeExpression
    {
        public VariableRangeExpression()
        {
            Type = "variableRange";
            TypeName = nameof(VariableRangeExpression);
        }

        public override string ToString()
        {
            return $"{First}..{Last}";
        }
    }

    public partial class VariableListExpression
    {
        public VariableListExpression()
        {
            Type = "variableList";
            TypeName = nameof(VariableListExpression);
        }

        public override string ToString()
        {
            // TODO
            return string.Join(" ", this.VariableNames);
        }
    }

    public partial class AdditionExpression : FunctionCallExpression
    {
        public AdditionExpression()
        {
            Function = "add";
            ProprietaryOperator = "+";
        }

        public override string ToString()
        {
            return $"{Arguments[0]} + {Arguments[1]}";
        }
    }

    public partial class SubtractionExpression : FunctionCallExpression
    {
        public SubtractionExpression()
        {
            Function = "subtract";
            ProprietaryOperator = "-";
        }

        public override string ToString()
        {
            return $"{Arguments[0]} - {Arguments[1]}";
        }
    }

    public partial class MultiplicationExpression : FunctionCallExpression
    {
        public MultiplicationExpression()
        {
            Function = "multiply";
            ProprietaryOperator = "*";
        }

        public override string ToString()
        {
            return $"{Arguments[0]} × {Arguments[1]}";
        }
    }

    public partial class DivisionExpression : FunctionCallExpression
    {
        public DivisionExpression()
        {
            Function = "divide";
            ProprietaryOperator = "/";
        }

        public override string ToString()
        {
            return $"{Arguments[0]} ÷ {Arguments[1]}";
        }
    }

    public partial class ExponentExpression : FunctionCallExpression
    {
        public ExponentExpression()
        {
            Function = "exponent";
            ProprietaryOperator = "**";
        }

        public override string ToString()
        {
            return $"{Arguments[0]} ^ {Arguments[1]}";
        }
    }

    public partial class FunctionCallExpression
    {
        public FunctionCallExpression()
        {
            TypeName = nameof(FunctionCallExpression);
        }

        public override string ToString()
        {
            string paramStr = string.Join(", ", Arguments);
            return $"{Name}({paramStr})";
        }
    }

    public partial class GroupedExpression
    {
        public string Function { get; set; } = "parentheses";

        public GroupedExpression()
        {
            TypeName = nameof(GroupedExpression);
        }

        public override string ToString()
        {
            return $"({Expression})";
        }
    }


}
