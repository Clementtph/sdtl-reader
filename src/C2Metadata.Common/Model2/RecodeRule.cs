using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Describes how values will be recoded.

    /// <summary>
    public partial class RecodeRule
    {
        /// <summary>
        /// The values to be recoded.
        /// <summary>
        public List<string> FromValue { get; set; } = new List<string>();
        public bool ShouldSerializeFromValue() { return FromValue.Count > 0; }
        /// <summary>
        /// The ranges of values to be recoded.
        /// <summary>
        public List<ValueRange> FromValueRange { get; set; } = new List<ValueRange>();
        public bool ShouldSerializeFromValueRange() { return FromValueRange.Count > 0; }
        /// <summary>
        /// Indicates the type of value that is recoded ('unhandled', 'lowest', 'highest', 'missing')
        /// <summary>
        public string SpecialFromValue { get; set; }
        /// <summary>
        /// The new value
        /// <summary>
        public string To { get; set; }
        /// <summary>
        /// Indicates how the value is recoded ('copy', 'missing')
        /// <summary>
        public string SpecialToValue { get; set; }
        /// <summary>
        /// A value label for the new recoded value, if appropriate
        /// <summary>
        public string Label { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            if (FromValue != null && FromValue.Count > 0)
            {
                xEl.Add(
                    from item in FromValue
                    select new XElement(ns + "FromValue", item.ToString()));
            }
            if (FromValueRange != null && FromValueRange.Count > 0)
            {
                foreach (var item in FromValueRange)
                {
                    xEl.Add(item.ToXml("FromValueRange"));
                }
            }
            if (SpecialFromValue != null)
            {
                xEl.Add(new XElement(ns + "SpecialFromValue", SpecialFromValue));
            }
            if (To != null)
            {
                xEl.Add(new XElement(ns + "To", To));
            }
            if (SpecialToValue != null)
            {
                xEl.Add(new XElement(ns + "SpecialToValue", SpecialToValue));
            }
            if (Label != null)
            {
                xEl.Add(new XElement(ns + "Label", Label));
            }
            return xEl;
        }
    }
}

