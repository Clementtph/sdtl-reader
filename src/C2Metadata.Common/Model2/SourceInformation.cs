using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// SourceInformation defines information about the original source of a data transform.

    /// <summary>
    public partial class SourceInformation
    {
        /// <summary>
        /// The line number of the beginning of the transform code
        /// <summary>
        public int LineNumberStart { get; set; }
        /// <summary>
        /// The line number of the end of the transform code
        /// <summary>
        public int LineNumberEnd { get; set; }
        /// <summary>
        /// The character index of the beginning of the transform code
        /// <summary>
        public int SourceStartIndex { get; set; }
        /// <summary>
        /// The character index of the end of the transform code
        /// <summary>
        public int SourceStopIndex { get; set; }
        /// <summary>
        /// The original source code of the data transform code
        /// <summary>
        public string OriginalSourceText { get; set; }
        /// <summary>
        /// The source code of the data transform code after processing macros or loops
        /// <summary>
        public string ProcessedSourceText { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            xEl.Add(new XElement(ns + "LineNumberStart", LineNumberStart));
            xEl.Add(new XElement(ns + "LineNumberEnd", LineNumberEnd));
            xEl.Add(new XElement(ns + "SourceStartIndex", SourceStartIndex));
            xEl.Add(new XElement(ns + "SourceStopIndex", SourceStopIndex));
            if (OriginalSourceText != null)
            {
                xEl.Add(new XElement(ns + "OriginalSourceText", OriginalSourceText));
            }
            if (ProcessedSourceText != null)
            {
                xEl.Add(new XElement(ns + "ProcessedSourceText", ProcessedSourceText));
            }
            return xEl;
        }
    }
}

