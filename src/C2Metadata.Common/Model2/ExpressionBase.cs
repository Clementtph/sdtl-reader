using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TODO

    /// <summary>
    public abstract partial class ExpressionBase
    {
        /// <summary>
        /// The name of the argument, when the expression is passed to a function.
        /// <summary>
        public string Name { get; set; }
        /// <summary>
        /// The name of this class, used for deserialization of derived types.
        /// <summary>
        [JsonProperty("$type")]
        public string TypeName { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            if (Name != null)
            {
                xEl.Add(new XElement(ns + "Name", Name));
            }
            if (TypeName != null)
            {
                xEl.Add(new XElement(ns + "TypeName", TypeName));
            }
            return xEl;
        }
    }
}

