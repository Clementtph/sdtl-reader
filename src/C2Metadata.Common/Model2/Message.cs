using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// Represents information about the parsing process.

    /// <summary>
    public partial class Message
    {
        /// <summary>
        /// Information, Warning, Error
        /// <summary>
        public string Severity { get; set; }
        /// <summary>
        /// The line number of the source that the messages is related to, if relevant.
        /// <summary>
        public int LineNumber { get; set; }
        /// <summary>
        /// The character position of the source that the message is related to, if relevant.
        /// <summary>
        public int CharacterPosition { get; set; }
        /// <summary>
        /// The content of the message.
        /// <summary>
        public string MessageText { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public virtual XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            if (Severity != null)
            {
                xEl.Add(new XElement(ns + "Severity", Severity));
            }
            xEl.Add(new XElement(ns + "LineNumber", LineNumber));
            xEl.Add(new XElement(ns + "CharacterPosition", CharacterPosition));
            if (MessageText != null)
            {
                xEl.Add(new XElement(ns + "MessageText", MessageText));
            }
            return xEl;
        }
    }
}

