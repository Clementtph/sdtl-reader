using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TODO

    /// <summary>
    public partial class Delete : TransformBase
    {
        /// <summary>
        /// The list of variables removed from the dataset.
        /// <summary>
        public List<string> Variables { get; set; } = new List<string>();
        public bool ShouldSerializeVariables() { return Variables.Count > 0; }
        /// <summary>
        /// The range of variables to be removed from the dataset.
        /// <summary>
        public List<VariableRangeExpression> VariableRange { get; set; } = new List<VariableRangeExpression>();
        public bool ShouldSerializeVariableRange() { return VariableRange.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variables != null && Variables.Count > 0)
            {
                xEl.Add(
                    from item in Variables
                    select new XElement(ns + "Variables", item.ToString()));
            }
            if (VariableRange != null && VariableRange.Count > 0)
            {
                foreach (var item in VariableRange)
                {
                    xEl.Add(item.ToXml("VariableRange"));
                }
            }
            return xEl;
        }
    }
}

