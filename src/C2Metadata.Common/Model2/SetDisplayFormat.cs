using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TODO

    /// <summary>
    public partial class SetDisplayFormat : TransformBase
    {
        /// <summary>
        /// The list of variables that will have their format set
        /// <summary>
        public List<string> Variables { get; set; } = new List<string>();
        public bool ShouldSerializeVariables() { return Variables.Count > 0; }
        /// <summary>
        /// The range of variables, each of which is to have the format assigned
        /// <summary>
        public VariableRangeExpression VariableRange { get; set; }
        /// <summary>
        /// The name of the format to be assigned to the variable(s). This name is system dependent.
        /// <summary>
        public string Format { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Variables != null && Variables.Count > 0)
            {
                xEl.Add(
                    from item in Variables
                    select new XElement(ns + "Variables", item.ToString()));
            }
            if (VariableRange != null) { xEl.Add(VariableRange.ToXml("VariableRange")); }
            if (Format != null)
            {
                xEl.Add(new XElement(ns + "Format", Format));
            }
            return xEl;
        }
    }
}

