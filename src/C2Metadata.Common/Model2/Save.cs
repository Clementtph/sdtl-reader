using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TODO

    /// <summary>
    public partial class Save : TransformBase
    {
        /// <summary>
        /// The name of the file to be saved.
        /// <summary>
        public string FileName { get; set; }
        /// <summary>
        /// The name of the file format, or the software package that works with the file.
        /// <summary>
        public string Software { get; set; }
        /// <summary>
        /// Indicates whether the file format is compressed.
        /// <summary>
        public bool IsCompressed { get; set; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("TransformBase").Descendants())
            {
                xEl.Add(el);
            }
            if (FileName != null)
            {
                xEl.Add(new XElement(ns + "FileName", FileName));
            }
            if (Software != null)
            {
                xEl.Add(new XElement(ns + "Software", Software));
            }
            xEl.Add(new XElement(ns + "IsCompressed", IsCompressed));
            return xEl;
        }
    }
}

