using System;
using System.Linq;
using Newtonsoft.Json;
using System.Xml.Linq;
using Cogs.SimpleTypes;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using Cogs.DataAnnotations;
using Cogs.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace sdtl
{
    /// <summary>
    /// TODO

    /// <summary>
    public partial class FunctionCallExpression : ExpressionBase
    {
        /// <summary>
        /// The name of the function being called.
        /// <summary>
        public string Function { get; set; }
        /// <summary>
        /// When the source language used an infix or prefix operator to invoke the function, this is the symbol that represents that operator.
        /// <summary>
        public string ProprietaryOperator { get; set; }
        /// <summary>
        /// When true, the Function property contains the name of a system-specfied function. When false, the Function property contains the name of a harmonized function as documented at TODO.
        /// <summary>
        public bool IsProprietary { get; set; }
        /// <summary>
        /// A list of parameters to the function.
        /// <summary>
        public List<ExpressionBase> Arguments { get; set; } = new List<ExpressionBase>();
        public bool ShouldSerializeArguments() { return Arguments.Count > 0; }

        /// <summary>
        /// Used to Serialize this object to XML
        /// <summary>
        public override XElement ToXml(string name)
        {
            XNamespace ns = "http://example.org/sdtl";
            XElement xEl = new XElement(ns + name);
            foreach (var el in base.ToXml("ExpressionBase").Descendants())
            {
                xEl.Add(el);
            }
            if (Function != null)
            {
                xEl.Add(new XElement(ns + "Function", Function));
            }
            if (ProprietaryOperator != null)
            {
                xEl.Add(new XElement(ns + "ProprietaryOperator", ProprietaryOperator));
            }
            xEl.Add(new XElement(ns + "IsProprietary", IsProprietary));
            if (Arguments != null && Arguments.Count > 0)
            {
                foreach (var item in Arguments)
                {
                    xEl.Add(item.ToXml("Arguments"));
                }
            }
            return xEl;
        }
    }
}

