﻿using C2Metadata.SpssToSdtl.Grammar;
using System;
using System.Collections.Generic;
using System.Text;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using System.Linq;
using sdtl;
using C2Metadata.Common.Model;

namespace C2Metadata.Common.SpssConverter
{
    public class CommandVisitor : SpssBaseVisitor<TransformBase>
    {
        public string SourceFileName { get; internal set; }

        public List<Message> Messages { get; } = new List<Message>();

        public override TransformBase VisitProgram([NotNull] SpssParser.ProgramContext context)
        {
            return base.VisitProgram(context);
        }

        public override TransformBase VisitChildren(IRuleNode node)
        {
            var commandList = new CommandList();

            if (node == null)
            {
                return commandList;
            }

            int n = node.ChildCount;
            for (int i = 0; i < n; i++)
            {
                if (!ShouldVisitNextChild(node, commandList))
                {
                    break;
                }

                var c = node.GetChild(i);
                var childResult = c.Accept(this);
                AggregateResult(commandList, childResult);
            }

            return commandList;
        }

        protected override TransformBase AggregateResult(TransformBase aggregate, TransformBase nextResult)
        {
            if (nextResult == null)
            {
                return aggregate;
            }

            var mainList = aggregate as CommandList;
            if (mainList != null)
            {
                var newList = nextResult as CommandList;
                if (newList != null)
                {
                    foreach (var child in newList.Commands)
                    {
                        if (child != null)
                        {
                            mainList.Commands.Add(child);
                        }
                    }
                }
                else
                {
                    mainList.Commands.Add(nextResult);
                }
            }

            return mainList;
        }

        public override TransformBase VisitTitle_command([NotNull] SpssParser.Title_commandContext context)
        {
            string title = context.title?.Text.Trim(new char[] { '"' });
            if (context.title?.StartIndex < 0)
            {
                var invalid = new Invalid
                {
                    Message = "TITLE command is missing the title"
                };

                return invalid;
            }

            var result = new SetDatasetProperty();
            result.PropertyName = "Title";
            result.Value = title;

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitSubtitle_command([NotNull] SpssParser.Subtitle_commandContext context)
        {
            string subtitle = context.subtitle?.Text.Trim(new char[] { '"' });
            if (context.subtitle?.StartIndex < 0)
            {
                var invalid = new Invalid
                {
                    Message = "SUBTITLE command is missing the subtitle"
                };

                return invalid;
            }

            var result = new SetDatasetProperty();
            result.PropertyName = "Subtitle";
            result.Value = subtitle;

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitSave_command([NotNull] SpssParser.Save_commandContext context)
        {
            if (context.outfile == null)
            {
                var invalid = new Invalid
                {
                    Message = "SAVE command is missing OUTFILE"
                };

                return invalid;
            }

            if (context.compressed != null &&
                context.uncompressed != null)
            {
                var invalid = new Invalid
                {
                    Message = "SAVE command cannot specify both COMPRESSED and UNCOMPRESSED"
                };

                return invalid;
            }

            var result = new Save();
            result.FileName = context.outfile.Text.Trim(new char[] { '"' });
            result.IsCompressed = context.compressed != null;

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitRecode_command([NotNull] SpssParser.Recode_commandContext context)
        {
            if (context.varlist == null)
            {
                var invalid = new Invalid
                {
                    Message = "RECODE command is missing varlist"
                };

                return invalid;
            }

            var result = new Recode();

            string[] varlistIds = context.varlist.IDENTIFIER()
                .Select(x => x.GetText())
                .ToArray();

            string[] intoIds = context.intovars?.IDENTIFIER()
                .Select(x => x.GetText())
                .ToArray();

            if (intoIds != null &&
                varlistIds.Length != intoIds.Length)
            {
                var invalid = new Invalid
                {
                    Message = "RECODE varlist must be the same length as the INTO list."
                };

                return invalid;
            }

            // varlist and matching with INTO variables
            for (int i = 0; i < context.varlist.ChildCount; i++)
            {
                var varlistItem = context.varlist.IDENTIFIER(i);

                string text = varlistItem.GetText();
                var recodeVariable = new RecodeVariable();
                recodeVariable.Source = varlistItem.GetText();

                if (intoIds != null)
                {
                    recodeVariable.Target = intoIds[i];
                }
                else
                {
                    recodeVariable.Target = recodeVariable.Source;
                }

                result.RecodedVariables.Add(recodeVariable);
            }

            // Rules
            for (int ruleIdx = 0; ruleIdx < context.numeric_value_list().Length; ruleIdx++)
            {
                var rule = new RecodeRule();

                // From
                var valueList = context.numeric_value_list(ruleIdx).numeric_value_list_member();
                foreach (var valueNode in valueList)
                {
                    if (valueNode.numeric_input_value()?.ELSE() != null)
                    {
                        rule.SpecialFromValue = "unhandled";
                    }
                    else if (valueNode.numeric_input_value() != null)
                    {
                        rule.FromValue.Add(valueNode.numeric_input_value().GetText());
                    }
                    else if (valueNode.thru() != null)
                    {
                        string low = valueNode.thru().lowValue.GetText();
                        string high = valueNode.thru().highValue.GetText();
                        var range = new ValueRange
                        {
                            First = low,
                            Last = high
                        };
                        rule.FromValueRange.Add(range);
                    }
                }

                // To
                var numericOutput = context.numeric_output(ruleIdx);
                if (numericOutput.COPY() != null)
                {
                    rule.SpecialToValue = "copy";
                }
                else
                {
                    string outputStr = numericOutput.GetText();
                    rule.To = outputStr;
                }

                result.Rules.Add(rule);
            }

            for (int ruleIdx = 0; ruleIdx < context.string_value_list().Length; ruleIdx++)
            {
                var rule = new RecodeRule();

                // From
                var valueList = context.string_value_list(ruleIdx).string_input_value();
                foreach (var valueNode in valueList)
                {
                    if (valueNode?.ELSE() != null)
                    {
                        rule.SpecialFromValue = "unhandled";
                    }
                    else if (valueNode != null)
                    {
                        rule.FromValue.Add(valueNode.GetText());
                    }
                }

                // To (string)
                var stringOutput = context.string_output(ruleIdx);
                if (stringOutput?.COPY() != null)
                {
                    rule.SpecialToValue = "copy";
                }
                else if (stringOutput != null)
                {
                    string outputStr = stringOutput.GetText();
                    rule.To = outputStr;
                }

                // To (numeric)
                var numericOutput = context.numeric_output(ruleIdx);
                if (numericOutput.COPY() != null)
                {
                    rule.SpecialToValue = "copy";
                }
                else if (numericOutput != null)
                {
                    string outputStr = numericOutput.GetText();
                    rule.To = outputStr;
                }

                result.Rules.Add(rule);
            }


            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitRename_command([NotNull] SpssParser.Rename_commandContext context)
        {
            if (context.pairs == null)
            {
                var invalid = new Invalid
                {
                    Message = "RENAME command is missing varlist"
                };

                return invalid;
            }

            var result = new Rename();

            foreach (var pairContext in context.children.OfType<SpssParser.Rename_pairContext>())
            {
                var pair = new RenamePair();
                result.Renames.Add(pair);

                if (pairContext.oldname != null)
                {
                    pair.OldName = pairContext.oldname.Text;
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "RENAME: cannot detect old name"
                    });
                }

                if (pairContext.newname != null)
                {
                    pair.NewName = pairContext.newname.Text;
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "RENAME: cannot detect new name"
                    });
                }

            }

            RecordSourceInformation(result, context);
            return result;
        }

        //public override TransformBase VisitExecute_command([NotNull] SpssParser.Execute_commandContext context)
        //{
        //    var result = new Execute();
        //    RecordSourceInformation(result, context);
        //    return result;
        //}

        public override TransformBase VisitGet_command([NotNull] SpssParser.Get_commandContext context)
        {
            if (context.filename == null)
            {
                var invalid = new Invalid
                {
                    Message = "GET command is missing FILE"
                };

                return invalid;
            }

            var result = new Load();
            result.FileName = context.filename.Text.Trim(new char[] { '"' });

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitCompute_command([NotNull] SpssParser.Compute_commandContext context)
        {
            // TODO Check for validity.

            var result = new Compute();

            result.Variable = context.variable.Text;
            result.Expression = ParseExpression(context.expression);

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitVariable_labels_command([NotNull] SpssParser.Variable_labels_commandContext context)
        {
            var result = new SetVariableLabel();

            result.VariableName = context.variable?.Text;

            if (!string.IsNullOrWhiteSpace(context.label?.Text))
            {
                result.Label = StripQuotes(context.label.Text);
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitValue_labels_command([NotNull] SpssParser.Value_labels_commandContext context)
        {
            var result = new SetValueLabels();

            if (context.variable != null)
            {
                result.Variables.Add(context.variable.Text);
            }
            else
            {
                AddMessage("Warning", "Set Value Label: no variable name found");
            }

            int pairCount = context.NUMERIC_LITERAL().Length;
            for (int i = 0; i < pairCount; i++)
            {
                string value = context.NUMERIC_LITERAL()[i].GetText();

                string label = string.Empty;
                if (context.STRING().Length > i)
                {
                    label = StripQuotes(context.STRING()[i]?.GetText());
                }

                result.Labels.Add(new ValueLabel
                {
                    Value = value,
                    Label = label
                });
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitPrint_format_command([NotNull] SpssParser.Print_format_commandContext context)
        {
            var result = new SetDisplayFormat();

            if (context.variable != null)
            {
                result.Variables.Add(context.variable.Text);
            }
            else
            {
                AddMessage("Warning", "Set Display Format: no variable name");
            }

            if (context.formatName != null)
            {
                result.Format = context.formatName.Text;
            }
            else
            {
                AddMessage("Warning", "Set Display Format: no format name");
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitMissing_values_command([NotNull] SpssParser.Missing_values_commandContext context)
        {
            var result = new CommandList();

            var count = context.var_list().Length;
            for (int i = 0; i < count; i++)
            {
                var command = new SetMissingValues();
                result.Commands.Add(command);

                string[] variableNames = context.var_list()[i].IDENTIFIER()
                    .Select(x => x.GetText())
                    .ToArray();
                command.Variables.AddRange(variableNames);

                if (context.numeric_value_list().Length > i)
                {
                    var valueList = context.numeric_value_list()[i].numeric_value_list_member();
                    foreach (var valueNode in valueList)
                    {
                        if (valueNode.numeric_input_value() != null)
                        {
                            command.Value.Add(valueNode.numeric_input_value().GetText());
                        }
                        else if (valueNode.thru() != null)
                        {
                            string low = valueNode.thru().lowValue.GetText();
                            string high = valueNode.thru().highValue.GetText();
                            var range = new ValueRange
                            {
                                First = low,
                                Last = high
                            };
                            command.ValueRange.Add(range);
                        }
                    }
                }

                RecordSourceInformation(command, context);

            }


            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitAdd_files_command([NotNull] SpssParser.Add_files_commandContext context)
        {
            var result = new MergeDatasets();

            foreach (ITerminalNode fileNameNode in context.STRING())
            {
                string fileName = StripQuotes(fileNameNode.GetText());
                result.FileName.Add(fileName);
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitDo_repeat_command([NotNull] SpssParser.Do_repeat_commandContext context)
        {
            // For every command within the loop, create a transform and set the information about which variables it applies to.
            var list = VisitChildren(context.commands);
            return list;
        }

        public override TransformBase VisitIf_command([NotNull] SpssParser.If_commandContext context)
        {
            var result = new Compute();

            result.Condition = ParseCondition(context.condition());
            result.Variable = context.variable?.Text;

            if (context.expression != null)
            {
                result.Expression = ParseExpression(context.expression);
            }

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitSelect_if_command([NotNull] SpssParser.Select_if_commandContext context)
        {
            var result = new Select();

            result.Condition = ParseCondition(context.condition());

            RecordSourceInformation(result, context);
            return result;
        }

        public override TransformBase VisitDelete_variables_command([NotNull] SpssParser.Delete_variables_commandContext context)
        {
            var result = new Delete();
            result.Command = "delete";

            foreach (var x in context.var_list_with_range().IDENTIFIER())
            {
                result.Variables.Add(x.GetText());
            }

            foreach (var x in context.var_list_with_range().var_range())
            {
                var range = new VariableRangeExpression();
                range.First = x.startVariable.Text;
                range.Last = x.endVariable.Text;

                result.VariableRange.Add(range);
            }

            RecordSourceInformation(result, context);
            return result;
        }

        private ExpressionBase ParseExpression(SpssParser.Transformation_expressionContext expression)
        {
            if (expression.OPEN_PAREN() != null &&
                expression.CLOSE_PAREN() != null)
            {
                var result = new GroupedExpression();

                if (expression.transformation_expression().Length > 0)
                {
                    var childExpression = expression.transformation_expression()[0];
                    var childParsed = ParseExpression(childExpression);
                    result.Expression = childParsed;
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = $"Grouped expression: cannot detect expressions"
                    });
                }

                return result;
            }

            else if (expression.PLUS() != null)
            {
                var result = new AdditionExpression();
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "PLUS");
                return result;
            }

            else if (expression.MINUS() != null)
            {
                var result = new SubtractionExpression();
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "MINUS");
                return result;
            }

            else if (expression.STAR() != null)
            {
                var result = new MultiplicationExpression();
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "STAR");
                return result;
            }

            else if (expression.SLASH() != null)
            {
                var result = new DivisionExpression();
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "SLASH");
                return result;
            }

            else if (expression.STARSTAR() != null)
            {
                var result = new ExponentExpression();
                HandleConditionsForFunctionCallTransformation(expression.transformation_expression(), result, "STARSTAR");
                return result;
            }

            else if (expression.function_call() != null)
            {
                var result = new FunctionCallExpression();
                result.Function = expression.function_call().IDENTIFIER().GetText();
                result.IsProprietary = true;

                var parameterList = expression.function_call().parameter_list();
                foreach (var param in parameterList.parameter())
                {
                    var expr = param.transformation_expression();
                    var varList = param.var_list();

                    if (expr != null)
                    {
                        var exprParam = ParseExpression(expr);
                        result.Arguments.Add(exprParam);
                    }

                    else if (varList != null)
                    {

                    }
                }

                return result;
            }

            else if (expression.IDENTIFIER() != null)
            {
                var result = new VariableSymbolExpression();
                result.VariableName = expression.IDENTIFIER().GetText();
                return result;
            }

            else if (expression.NUMERIC_LITERAL() != null)
            {
                string str = expression.NUMERIC_LITERAL().GetText();
                if (int.TryParse(str, out var number))
                {
                    var result = new IntConstantExpression();
                    result.IntValue = number;
                    return result;
                }
                else if (double.TryParse(str, out var dNumber))
                {
                    var result = new DoubleConstantExpression();
                    result.DoubleValue = number;
                    return result;
                }

                return null;
            }

            else if (expression.STRING() != null)
            {
                var result = new StringConstantExpression();
                string str = expression.STRING().GetText();

                // Remove first and last character (the quotes).
                str = StripQuotes(str);
                result.Value = str;
                return result;
            }

            return null;
        }

        private ExpressionBase ParseCondition(SpssParser.ConditionContext condition)
        {
            if (condition.AND() != null)
            {
                var result = new AndCondition();
                HandleConditionsForFunctionCallCondition(condition, result, "AND");
                return result;
            }

            else if (condition.OR() != null)
            {
                var result = new OrCondition();
                HandleConditionsForFunctionCallCondition(condition, result, "OR");
                return result;
            }

            else if (condition.NOT() != null)
            {
                var result = new NotCondition();

                if (condition.condition().Length > 0)
                {
                    result.Arguments.Add(ParseCondition(condition.condition()[0]));
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "NOT expression: cannot detect expression"
                    });
                }

                return result;
            }

            else if (condition.OPEN_PAREN() != null &&
                condition.CLOSE_PAREN() != null)
            {
                var result = new GroupedCondition();

                if (condition.condition().Length > 0)
                {
                    var childCondition = condition.condition()[0];
                    var childParsed = ParseCondition(childCondition);
                    result.Arguments.Add(childParsed);
                }
                else
                {
                    Messages.Add(new Message
                    {
                        Severity = "Warning",
                        MessageText = "Grouped expression: cannot detect expression"
                    });
                }

                return result;
            }

            else if (condition.EQUAL() != null)
            {
                if (condition.transformation_expression().Length < 2) return null;

                var result = new EqualCondition();
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "EQUAL");
                return result;
            }

            else if (condition.NOTEQUAL() != null)
            {
                var result = new NotEqualCondition();
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "NOTEQUAL");
                return result;
            }

            else if (condition.GT() != null)
            {
                var result = new GreaterThanCondition();
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "GreaterThan");
                return result;
            }

            else if (condition.GT_EQ() != null)
            {
                var result = new GreaterThanOrEqualCondition();
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "GreaterThanOrEqual");
                return result;
            }

            else if (condition.LT() != null)
            {
                var result = new LessThanCondition();
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "LessThan");
                return result;
            }

            else if (condition.LT_EQ() != null)
            {
                var result = new LessThanOrEqualCondition();
                HandleConditionsForFunctionCallTransformation(condition.transformation_expression(), result, "LessThanOrEqual");
                return result;
            }

            return null;
        }

        private void HandleConditionsForFunctionCallCondition(SpssParser.ConditionContext condition, FunctionCallExpression expr, string expressionType)
        {
            var conditionsTree = condition.condition();
            if (conditionsTree.Length > 0)
            {
                expr.Arguments.Add(ParseCondition(conditionsTree[0]));
            }
            if (conditionsTree.Length > 1)
            {
                expr.Arguments.Add(ParseCondition(conditionsTree[1]));
            }

            if (conditionsTree.Length < 2)
            {
                Messages.Add(new Message
                {
                    Severity = "Warning",
                    MessageText = $"{expressionType} expression: cannot detect expressions"
                });
            }
        }

        private void HandleConditionsForFunctionCallTransformation(SpssParser.Transformation_expressionContext[] expressionTree, FunctionCallExpression expr, string expressionType)
        {
            if (expressionTree.Length > 0)
            {
                expr.Arguments.Add(ParseExpression(expressionTree[0]));
            }
            if (expressionTree.Length > 1)
            {
                expr.Arguments.Add(ParseExpression(expressionTree[1]));
            }

            if (expressionTree.Length < 2)
            {
                Messages.Add(new Message
                {
                    Severity = "Warning",
                    MessageText = $"{expressionType} expression: cannot detect expressions"
                });
            }
        }

        private static string StripQuotes(string str)
        {
            str = str.Substring(1, str.Length - 2);
            return str;
        }

        private void RecordSourceInformation(TransformBase command, Antlr4.Runtime.ParserRuleContext context)
        {
            command.SourceInformation = new SourceInformation();
            command.SourceInformation.SourceStartIndex = context.Start.StartIndex;
            command.SourceInformation.SourceStopIndex = context.Stop.StopIndex;
            command.SourceInformation.LineNumberStart = context.Start.Line - 1;
            command.SourceInformation.LineNumberEnd = context.Stop.Line - 1;
            command.SourceInformation.OriginalSourceText = context.Start.InputStream
                .ToString()
                .Substring(context.Start.StartIndex, context.Stop.StopIndex - context.Start.StartIndex + 1)
                .Trim();
        }

        private void AddMessage(string severity, string text)
        {
            Messages.Add(new Message
            {
                Severity = severity,
                MessageText = text
            });
        }

    }
}
