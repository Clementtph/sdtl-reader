﻿using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Dfa;
using Antlr4.Runtime.Sharpen;
using Antlr4.Runtime.Tree;
using C2Metadata.Common.Model;
using C2Metadata.SpssToSdtl.Grammar;
using sdtl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using static C2Metadata.SpssToSdtl.Grammar.SpssParser;

namespace C2Metadata.Common.SpssConverter
{
    public class BasicConverter
    {
        private string fileName;
        private bool isVerbose;

        public BasicConverter()
        {

        }

        public Program ConvertFile(string fileName, bool isVerbose = false)
        {
            this.fileName = fileName;
            this.isVerbose = isVerbose;

            string content = File.ReadAllText(fileName);

            Program program = Convert(content);
            AddGeneralProgramInformation(program, content);

            program.SourceFileName = Path.GetFileName(fileName);
            var fileInfo = new FileInfo(fileName);
            program.SourceFileLastUpdate = fileInfo.LastWriteTimeUtc;
            program.SourceFileSize = fileInfo.Length;

            return program;
        }

        public Program ConvertString(string content, bool isVerbose = false)
        {
            this.isVerbose = isVerbose;
            Program program = Convert(content);
            AddGeneralProgramInformation(program, content);

            program.SourceFileName = string.Empty;
            program.SourceFileLastUpdate = GetUtcNowWithReasonablePrecision();
            program.SourceFileSize = content.Length;

            return program;
        }

        private void AddGeneralProgramInformation(Program program, string content)
        {
            if (program.Commands != null)
            {
                program.CommandCount = program.Commands.Count;
            }

            program.LineCount = content.Split('\n').Length;
            program.ModelCreatedTime = GetUtcNowWithReasonablePrecision();
            program.ModelVersion = "0.2 Development";
            program.Parser = "spss-to-sdtl";
            program.ParserVersion = "0.2 Development";
            program.ScriptMD5 = GetMd5Hash(content);
            program.ScriptSHA1 = GetSha1Hash(content);
        }

        private static DateTimeOffset GetUtcNowWithReasonablePrecision()
        {
            var now = DateTimeOffset.UtcNow;
            DateTimeOffset then = new DateTimeOffset(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, 0, TimeSpan.FromTicks(0));
            return then;
        }

        private static string GetMd5Hash(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        static string GetSha1Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }

        private Program Convert(string content)
        {
            // Prefix with a newline.
            content = '\n' + content + '\n';

            Program program = new Program();
            program.SourceLanguage = "spss";

            using (var reader = new MemoryStream(Encoding.UTF8.GetBytes(content ?? "")))
            {
                try
                {
                    // Tokenize and parse.
                    var inputStream = new AntlrInputStream(reader);
                    var lexer = new SpssLexer(inputStream);
                    var tokens = new CommonTokenStream(lexer);
                    var parser = new SpssParser(tokens);

                    var errorListener = new SpssErrorListener();
                    parser.AddErrorListener(errorListener);

                    ProgramContext tree = parser.program();

                    // Add messages from the parser.
                    foreach (var message in errorListener.Messages)
                    {
                        program.Messages.Add(message);
                    }

                    // Visit everything in tree to build the commands.
                    var commandVisitor = new CommandVisitor();
                    commandVisitor.SourceFileName = fileName;
                    var commandList = commandVisitor.VisitProgram(tree)
                        as CommandList;

                    // Add messages from the tree visitor.
                    foreach (var message in commandVisitor.Messages)
                    {
                        program.Messages.Add(message);
                    }

                    program.Commands.AddRange(commandList.Commands);

                    // Very verbose output, if requested.
                    if (isVerbose)
                    {
                        Console.WriteLine("Tokens");
                        Console.WriteLine("----------");
                        foreach (var token in tokens.GetTokens())
                        {
                            Console.WriteLine(token.ToString());
                        }

                        Console.WriteLine("Tree");
                        Console.WriteLine("----------");
                        foreach (var kid in tree.children)
                        {
                            Console.WriteLine(kid.ToStringTree(parser));
                        }
                    }

                    return program;
                }
                catch (Exception ex)
                {
                    program.Messages.Add(new Message
                    {
                        Severity = "Error",
                        MessageText = "Unhandled error. " + ex.Message
                    });

                    return program;
                }
            }
        }
    }

    public class SpssErrorListener : BaseErrorListener
    {
        public List<Message> Messages { get; } = new List<Message>();

        public override void SyntaxError(IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            var message = new Message
            {
                Severity = "Error",
                LineNumber = line - 1,
                CharacterPosition = charPositionInLine,
                MessageText = msg
            };
            Messages.Add(message);
        }

        public override void ReportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, bool exact, BitSet ambigAlts, ATNConfigSet configs)
        {
            
        }

        public override void ReportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, SimulatorState acceptState)
        {
            
        }

        public override void ReportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, SimulatorState conflictState)
        {
            
        }
    }
}
