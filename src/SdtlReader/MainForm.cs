﻿// Copyright 2017 Colectica.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using Eto.Forms;
using Eto.Drawing;
using System.Collections.Generic;
using SdtlReader.CommandViews;
using SdtlReader.Views;
using sdtl;
using C2Metadata.Common.Model;

namespace SdtlReader
{
    public class MainForm : Form
    {
        private TreeView transformsTree;
        private Splitter splitter;

        public ShellViewModel ViewModel { get; set; }

        public MainForm()
        {
            Title = "C2Metadata Transform Reader";
            ClientSize = new Size(900, 600);

            // Commands
            var openCommand = new Command { MenuText = "&Open", ToolBarText = "Open", Shortcut = Application.Instance.CommonModifier | Keys.O };
            openCommand.Executed += (sender, e) => ViewModel.OpenFile();

            var exportJsonCommand = new Command { MenuText = "Export &JSON", ToolBarText = "Export JSON", Shortcut = Application.Instance.CommonModifier | Keys.J };
            exportJsonCommand.Executed += (sender, e) => ViewModel.SaveJson();

            var exportXmlCommand = new Command { MenuText = "Export &XML", ToolBarText = "Export XML", Shortcut = Application.Instance.CommonModifier | Keys.X };
            exportXmlCommand.Executed += (sender, e) => ViewModel.SaveXml();

            var quitCommand = new Command { MenuText = "Exit", Shortcut = Application.Instance.CommonModifier | Keys.F4 };
            quitCommand.Executed += (sender, e) => Application.Instance.Quit();

            var aboutCommand = new Command { MenuText = "About..." };
            aboutCommand.Executed += (sender, e) => MessageBox.Show(this,
                @"C2Metadata Transform Reader 
Copyright © 2017 Colectica. All rights reserved.

Licensed under the Apache License, Version 2.0 (the ""License"");
you may not use this software except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an ""AS IS"" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and
limitations under the License.

C2Metadata is supported by Data Infrastructure Building Blocks 
(DIBBs) program of the National Science Foundation through 
grant NSF ACI-1640575.
",
                "About C2Metadata Transform Reader",
                MessageBoxType.Information);

            // Menu
            Menu = new MenuBar
            {
                Items =
                {
					// File submenu
					new ButtonMenuItem { Text = "&File", Items = { openCommand, exportJsonCommand, exportXmlCommand } },
					// new ButtonMenuItem { Text = "&Edit", Items = { /* commands/items */ } },
					// new ButtonMenuItem { Text = "&View", Items = { /* commands/items */ } },
				},
                ApplicationItems =
                {
                },
                QuitItem = quitCommand,
                AboutItem = aboutCommand
            };

            // Toolbar
            ToolBar = new ToolBar { Items = { openCommand, exportJsonCommand, exportXmlCommand } };

            // Left side: List of transforms
            transformsTree = new TreeView();
            transformsTree.Size = new Size(300, 300);
            transformsTree.SelectionChanged += TransformsTree_SelectionChanged;

            // Right side: Transform details
            var detailsView = new StackLayout();

            // Splitter
            splitter = new Splitter()
            {
                Panel1 = transformsTree,
                Panel2 = new Label { Text = "Use the Open button to get started." },
                FixedPanel = SplitterFixedPanel.Panel1
            };

            Content = splitter;
        }

        private void TransformsTree_SelectionChanged(object sender, EventArgs e)
        {
            if (transformsTree.SelectedItem is TreeItem treeItem)
            {
                object tag = treeItem.Tag;
                if (tag is Program program)
                {
                    UpdateDetailView(program);
                }
                else if (tag is TransformBase transform)
                {
                    UpdateDetailView(transform);
                }
                else
                {
                    UpdateDetailView();
                }
            }
        }

        public void UpdateTransformsTree()
        {
            var rootItem = new TreeItem();
            rootItem.Text = "Root";

            var summaryItem = new TreeItem();
            summaryItem.Text = "Summary";
            rootItem.Children.Add(summaryItem);

            foreach (var sequence in ViewModel.Programs)
            {
                // Add the sequence of transforms.
                var sequenceItem = new TreeItem();
                sequenceItem.Text = System.IO.Path.GetFileName(sequence.SourceFileName);
                sequenceItem.Tag = sequence;
                rootItem.Children.Add(sequenceItem);

                // Add each transform.
                foreach (var transform in sequence.Commands)
                {
                    var transformItem = new TreeItem();
                    transformItem.Text = transform.Command;
                    transformItem.Tag = transform;
                    sequenceItem.Children.Add(transformItem);
                }
            }

            transformsTree.DataStore = rootItem;
        }

        public void UpdateDetailView()
        {
            splitter.Panel2 = new Label { Text = "Select an item on the left." };
        }

        public void UpdateDetailView(TransformBase transform)
        {
            if (transform == null)
            {
                UpdateDetailView();
                return;
            }

            var map = new Dictionary<Type, Func<TransformBase, Panel>>()
            {
                { typeof(Comment), t => new CommentControl(t as Comment) },
                { typeof(Compute), t => new ComputeControl(t as Compute) },
                { typeof(Delete), t => new DeleteControl(t as Delete) },
                { typeof(DoIf), t => new DoIfControl(t as DoIf) },
                { typeof(Execute), t => new ExecuteControl(t as Execute) },
                { typeof(Invalid), t => new InvalidControl(t as Invalid) },
                { typeof(KeepVariables), t => new KeepVariablesControl(t as KeepVariables) },
                { typeof(Load), t => new LoadControl(t as Load) },
                { typeof(MergeDatasets), t => new MergeDatasetsControl(t as MergeDatasets) },
                { typeof(Recode), t => new RecodeControl(t as Recode) },
                { typeof(Rename), t => new RenameControl(t as Rename) },
                { typeof(ReshapeLong), t => new ReshapeLongControl(t as ReshapeLong) },
                { typeof(ReshapeWide), t => new ReshapeWideControl(t as ReshapeWide) },
                { typeof(Save), t => new SaveControl(t as Save) },
                { typeof(Select), t => new SelectControl(t as Select) },
                { typeof(SetDatasetProperty), t => new SetDatasetPropertyControl(t as SetDatasetProperty) },
                { typeof(SetDisplayFormat), t => new SetDisplayFormatControl(t as SetDisplayFormat) },
                { typeof(SetMissingValues), t => new SetMissingValuesControl(t as SetMissingValues) },
                { typeof(SetValueLabels), t => new SetValueLabelsControl(t as SetValueLabels) },
                { typeof(SetVariableLabel), t => new SetVariableLabelControl(t as SetVariableLabel) },
            };

            var type = transform.GetType();
            if (map.ContainsKey(type))
            {
                splitter.Panel2 = map[type](transform);
            }

        }

        private void UpdateDetailView(Program program)
        {
            splitter.Panel2 = new TransformSequenceSummaryView(program);
        }

    }
}