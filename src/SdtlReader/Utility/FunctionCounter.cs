﻿using sdtl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.Utility
{
    public class FunctionCounter
    {
        private Dictionary<string, int> result;

        public Dictionary<string, int> CountFunctions(IEnumerable<TransformBase> commands)
        {
            this.result = new Dictionary<string, int>();

            foreach (var transform in commands)
            {
                if (transform is Compute compute)
                {
                    ProcessExpression(compute.Expression);
                }
            }

            return result;
        }

        private void ProcessExpression(ExpressionBase expression)
        {
            if (expression is FunctionCallExpression functionCall)
            {
                IncrementFunctionCall(functionCall.Name);

                foreach (var param in functionCall.Arguments)
                {
                    ProcessExpression(param);
                }
            }
            else if (expression is GroupedExpression grouped)
            {
                ProcessExpression(grouped.Expression);
            }
            else if (expression is AdditionExpression add)
            {
                ProcessExpression(add.Arguments[0]);
                ProcessExpression(add.Arguments[1]);
            }
            else if (expression is SubtractionExpression sub)
            {
                ProcessExpression(sub.Arguments[0]);
                ProcessExpression(sub.Arguments[1]);
            }
            else if (expression is MultiplicationExpression mult)
            {
                ProcessExpression(mult.Arguments[0]);
                ProcessExpression(mult.Arguments[1]);
            }
            else if (expression is DivisionExpression divide)
            {
                ProcessExpression(divide.Arguments[0]);
                ProcessExpression(divide.Arguments[1]);
            }
            else if (expression is ExponentExpression exp)
            {
                ProcessExpression(exp.Arguments[0]);
                ProcessExpression(exp.Arguments[1]);
            }

        }

        private void IncrementFunctionCall(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return;
            }

            if (!result.ContainsKey(name))
            {
                result.Add(name, 1);
            }
            else
            {
                result[name]++;
            }

        }
    }
}
