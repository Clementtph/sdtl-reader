﻿// Copyright 2017 Colectica.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using Eto.Forms;
using sdtl;
using SdtlReader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.CommandViews
{
    public class ReshapeLongControl : Panel
    {
        private ReshapeLong reshapeLong;

        public ReshapeLongControl(ReshapeLong reshapeLong)
        {
            this.reshapeLong = reshapeLong;

            Content = UIBuilder.MakeScrollable(
                UIBuilder.CreateKeyValueTable(
                    "Make Items", string.Join(", ", reshapeLong.MakeItems),
                    "Index Values", string.Join(", ", reshapeLong.IndexValues),
                    "Index Variable", reshapeLong.IndexVarName
                ),
                UIBuilder.CreateSourceInformationCells(reshapeLong.SourceInformation)
            );
        }
    }
}
