﻿// Copyright 2017 Colectica.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using Eto.Forms;
using sdtl;
using SdtlReader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.CommandViews
{
    public class SetValueLabelsControl : Panel
    {
        private SetValueLabels setValueLabels;

        public SetValueLabelsControl(SetValueLabels setValueLabels)
        {
            this.setValueLabels = setValueLabels;

            Content = UIBuilder.MakeScrollable(
                UIBuilder.CreateKeyValueTable(
                    "Variables", string.Join(", ", setValueLabels.Variables),
                    "Variable Range", setValueLabels.VariableRange?.ToString(),
                    "Labels", string.Join(", ", setValueLabels.Labels
                        .Select(x => $"{x.Value} = {x.Label}"))
                ),
                UIBuilder.CreateSourceInformationCells(setValueLabels.SourceInformation)
            );
        }
    }
}
