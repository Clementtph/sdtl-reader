﻿// Copyright 2017 Colectica.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using Eto.Forms;
using sdtl;
using SdtlReader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SdtlReader.CommandViews
{
    public class KeepVariablesControl : Panel
    {
        private KeepVariables keepVariables;

        public KeepVariablesControl(KeepVariables keepVariables)
        {
            this.keepVariables = keepVariables;

            Content = UIBuilder.MakeScrollable(
                UIBuilder.CreateKeyValueTable(
                    "Variables", string.Join(", ", keepVariables.Variables),
                    "Variable Range", keepVariables.VariableRange?.ToString()
                ),
                UIBuilder.CreateSourceInformationCells(keepVariables.SourceInformation)
            );
        }
    }
}
