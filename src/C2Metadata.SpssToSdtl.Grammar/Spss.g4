grammar Spss;

// Parser Rules

program
    : (NEWLINE? command)* EOF
    ;

command:   
      recode_command
    | rename_command
    | title_command
    | subtitle_command
    | save_command
    | execute_command
    | get_command
    | compute_command
    | variable_labels_command
    | value_labels_command
    | print_format_command
    | missing_values_command
    | add_files_command
    | do_repeat_command
    | if_command
    | select_if_command
    | delete_variables_command
    ;

title_command: TITLE title=STRING END_COMMAND;
subtitle_command: SUBTITLE subtitle=STRING END_COMMAND;
execute_command: EXECUTE END_COMMAND;

save_command: SAVE 
    OUTFILE EQUAL outfile=STRING 
    ( SLASH compressed=COMPRESSED
    | SLASH uncompressed=UNCOMPRESSED
    )* 
    END_COMMAND;

get_command: GET FILE EQUAL filename=STRING END_COMMAND;

recode_command: 
    RECODE varlist=var_list (OPEN_PAREN inputs=numeric_value_list EQUAL output=numeric_output CLOSE_PAREN)+ (INTO intovars=var_list)?
    (SLASH var_list (OPEN_PAREN numeric_value_list EQUAL numeric_output CLOSE_PAREN)+ (INTO var_list)? )?
    END_COMMAND

    | RECODE varlist=var_list (OPEN_PAREN string_value_list EQUAL string_output CLOSE_PAREN)+ (INTO intovars=var_list)?
    (SLASH var_list (OPEN_PAREN string_value_list EQUAL string_output CLOSE_PAREN)+ (INTO var_list)?)?
    END_COMMAND

    | RECODE varlist=var_list (OPEN_PAREN string_value_list EQUAL output=numeric_output CLOSE_PAREN)+ INTO intovars=var_list?
    (SLASH var_list (OPEN_PAREN string_value_list EQUAL output=numeric_output CLOSE_PAREN)+ INTO var_list?)?
    END_COMMAND
    ;

compute_command:
    COMPUTE variable=IDENTIFIER EQUAL expression=transformation_expression END_COMMAND;

variable_labels_command:
    VARIABLE LABELS variable=IDENTIFIER label=STRING END_COMMAND;

value_labels_command:
    VALUE LABELS variable=IDENTIFIER (value=NUMERIC_LITERAL label=STRING)+ END_COMMAND;

print_format_command:
    PRINT FORMATS variable=IDENTIFIER OPEN_PAREN (formatName=.*) CLOSE_PAREN END_COMMAND;

missing_values_command:
    MISSING VALUES (varlist=var_list OPEN_PAREN numeric_value_list CLOSE_PAREN)+ END_COMMAND;

add_files_command:
    ADD FILES (SLASH FILE EQUAL fileName=STRING)+ END_COMMAND;

do_repeat_command:
    DO REPEAT iterator=IDENTIFIER EQUAL startVariable=IDENTIFIER TO endVariable=IDENTIFIER END_COMMAND
    commands=command*
    END REPEAT END_COMMAND;

if_command:
    IF condition variable=IDENTIFIER EQUAL expression=transformation_expression END_COMMAND;

select_if_command: SELECT IF condition END_COMMAND;

delete_variables_command: DELETE VARIABLES (var_list_with_range) END_COMMAND;

rename_command: RENAME VARIABLES (OPEN_PAREN? pairs=rename_pair CLOSE_PAREN?)+ END_COMMAND;
rename_pair: oldname=IDENTIFIER EQUAL newname=IDENTIFIER;

numeric_input_value: NUMERIC_LITERAL | LO | LOWEST | HI | HIGHEST | MISSING | SYSMIS | ELSE;
thru: lowValue=numeric_input_value THRU highValue=numeric_input_value;
numeric_value_list_member: numeric_input_value | thru;
numeric_value_list: numeric_value_list_member (COMMA? numeric_value_list_member)*;

string_input_value: STRING | CONVERT | ELSE;
string_value_list: string_input_value (COMMA? string_input_value)*;

numeric_output: NUMERIC_LITERAL | COPY | SYSMIS;
string_output: STRING | COPY;


// Expressions
transformation_expression: 
    NUMERIC_LITERAL
    | STRING
    | IDENTIFIER
    | transformation_expression STARSTAR transformation_expression
    | transformation_expression STAR transformation_expression
    | transformation_expression SLASH transformation_expression
    | transformation_expression PLUS transformation_expression
    | transformation_expression MINUS transformation_expression
    | function_call
    | OPEN_PAREN transformation_expression CLOSE_PAREN
    ;

condition:
    condition AND condition
    | condition OR condition
    | NOT condition
    | OPEN_PAREN condition CLOSE_PAREN
    | transformation_expression EQUAL transformation_expression
    | transformation_expression NOTEQUAL transformation_expression
    | transformation_expression GT transformation_expression
    | transformation_expression GT_EQ transformation_expression
    | transformation_expression LT transformation_expression
    | transformation_expression LT_EQ transformation_expression
    ;

function_call: IDENTIFIER OPEN_PAREN parameter_list CLOSE_PAREN;
parameter_list: parameter (COMMA parameter)*;
parameter:
    transformation_expression
    | var_list
    ;

var_list: IDENTIFIER+;
var_range: startVariable=IDENTIFIER TO endVariable=IDENTIFIER;
var_list_with_range: (IDENTIFIER | var_range)+;






// ***************************************************************
// Lexer Rules
// ***************************************************************


// Strings can use either single or double quotes.
// Strings are escaped by two quotes in a row.
STRING : '"' (~[\r\n"] | '""')* '"' 
       | '\'' (~[\r\n\'] | '\'\'')* '\'';

END_COMMAND: DOT [ \t]* [\r\n]
    | DOT [ \t]* '/*' ~[\r\n]* ;

NUMERIC_LITERAL: MINUS? DIGIT+ (DOT DIGIT+)?;

SINGLE_LINE_COMMENT
 : [\r\n]+ [ \t]* '*' ~[\r\n]* -> channel(HIDDEN)
 ;
SINGLE_LINE_COMMENT2
 : '/*' ~[\r\n]* -> channel(HIDDEN)
 ;




NEWLINE : [\r\n\u2028\u2029]+ -> skip;

ADD: A D D;
AND: A N D;
COMPRESSED: C O M P R E S S E D;
COMPUTE: C O M P U T E;
CONVERT: C O N V E R T;
COPY: C O P Y;
DELETE: D E L E T E;
DO: D O;
ELSE: E L S E;
END: E N D;
EXECUTE: E X E C U T E;
FILE: F I L E;
FILES: F I L E S;
FORMATS: F O R M A T S;
GET: G E T;
HI: H I;
HIGHEST: H I G H E S T;
IF: I F;
INTO: I N T O;
LABELS: L A B E L S;
LO: L O;
LOWEST: L O W E S T;
MISSING: M I S S I N G;
NOT: N O T;
OR: O R;
OUTFILE: O U T F I L E;
PRINT: P R I N T;
RECODE: R E C O D E;
RENAME: R E N A M E;
REPEAT: R E P E A T;
SAVE: S A V E;
SELECT: S E L E C T;
SUBTITLE: S U B T I T L E;
SYSMIS: S Y S M I S;
THRU: T H R U;
TITLE: T I T L E;
TO: T O;
UNCOMPRESSED: U N C O M P R E S S E D;
VALUE: V A L U E;
VALUES: V A L U E S;
VARIABLE: V A R I A B L E;
VARIABLES: V A R I A B L E S;

IDENTIFIER: [a-zA-Z_@] [a-zA-Z_@0-9]* (DOT [a-zA-Z_@0-9]+)? ;

DOT : '.';
SLASH : '/';
COMMA : ',';
SEMICOLON : ';';
OPEN_PAREN : '(';
CLOSE_PAREN : ')';
EQUAL : '=';
NOTEQUAL : '<>';
STAR : '*';
STARSTAR : '**';
PLUS : '+';
MINUS : '-';
TILDE : '~';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
PIPE : '|';
AMBERSAND : '&';

DIGIT : [0-9];

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];

SPACE : ' ' -> skip;
TAB : '\t' -> skip;