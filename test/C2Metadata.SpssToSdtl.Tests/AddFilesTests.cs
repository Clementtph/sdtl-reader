﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class AddFilesTests
    {
        [Fact]
        public void AddFilesLabel_UseCase1()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"add files
  /file = ""da7568-1_rename.sav""
  /file = ""da7568-2_rename.sav"".
");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<MergeDatasets>(commandList.Commands[0]);
            var command = commandList.Commands[0] as MergeDatasets;

            Assert.Equal("da7568-1_rename.sav", command.FileName[0]);
            Assert.Equal("da7568-2_rename.sav", command.FileName[1]);
        }
    }
}
