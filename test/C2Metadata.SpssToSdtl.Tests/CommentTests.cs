﻿using C2Metadata.Common.SpssConverter;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class CommentTests
    {
        [Fact]
        public void SingleLineComment()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("* This is a simple, single line comment.");
            Assert.Empty(commandList.Commands);
        }

        [Fact]
        public void SingleLineCommentWithOtherCommandsFollowing()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"* This is a simple, single line comment.

COMPUTE var1 = 10.");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void SingleLineSlashCommentWithOtherCommandsFollowing()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"/* This is a simple, single line comment.

COMPUTE var1 = 10.");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void EndOfLineComment()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"COMPUTE var1 = 10. /* Here is the comment");
            Assert.Single(commandList.Commands);
        }

        [Fact]
        public void SingleLineCommentWithNoTerminator()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("* This is a simple, single line comment");
            Assert.Empty(commandList.Commands);
        }

        [Fact]
        public void LineOfStars()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"******************************
/* just a test");
            Assert.Empty(commandList.Commands);
        }

        [Fact]
        public void DoubleAsterisk()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"**RECODE var1 (SYSMIS=99).**


*******test***************.");
            Assert.Empty(commandList.Commands);
        }
    }
}
