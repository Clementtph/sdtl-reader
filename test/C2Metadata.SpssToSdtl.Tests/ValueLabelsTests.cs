﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class ValueLabelsTests
    {
        [Fact]
        public void ValueLabels_UseCase1()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("value labels SEXRACE 1 'White Male' 2 'Black Male' 3 'White Female' 4 'Black Female'.");
            int count = commandList.Commands.Count;

            Assert.Equal(1, count);
            Assert.IsType<SetValueLabels>(commandList.Commands[0]);

            var command = commandList.Commands[0] as SetValueLabels;

            Assert.Equal("SEXRACE", command.VariableRange.First);

            Assert.Equal("1", command.Labels[0].Value);
            Assert.Equal("2", command.Labels[1].Value);
            Assert.Equal("3", command.Labels[2].Value);
            Assert.Equal("4", command.Labels[3].Value);

            Assert.Equal("White Male", command.Labels[0].Label);
            Assert.Equal("Black Male", command.Labels[1].Label);
            Assert.Equal("White Female", command.Labels[2].Label);
            Assert.Equal("Black Female", command.Labels[3].Label);
        }

        [Fact]
        public void ValueLabels_UseCase2()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"VALUE LABELS HHinc_cat 
  1.00 'Less than $25,000'
  2.00 '$25,000-$50,000'
  3.00 '$50,000-$75,000'
  4.00 '$75,000-$100,000'
  5.00 '$100,000-$150,000'
  6.00 'Greater than $150,000'.
");
            int count = commandList.Commands.Count;

            Assert.Equal(1, count);
            Assert.IsType<SetValueLabels>(commandList.Commands[0]);

            var command = commandList.Commands[0] as SetValueLabels;

            Assert.Equal("HHinc_cat", command.VariableRange.First);

            Assert.Equal("1.00", command.Labels[0].Value);
            Assert.Equal("2.00", command.Labels[1].Value);
            Assert.Equal("3.00", command.Labels[2].Value);
            Assert.Equal("4.00", command.Labels[3].Value);
            Assert.Equal("5.00", command.Labels[4].Value);
            Assert.Equal("6.00", command.Labels[5].Value);

            Assert.Equal("Less than $25,000", command.Labels[0].Label);
            Assert.Equal("$25,000-$50,000", command.Labels[1].Label);
            Assert.Equal("$50,000-$75,000", command.Labels[2].Label);
            Assert.Equal("$75,000-$100,000", command.Labels[3].Label);
            Assert.Equal("$100,000-$150,000", command.Labels[4].Label);
            Assert.Equal("Greater than $150,000", command.Labels[5].Label);

        }
    }
}
