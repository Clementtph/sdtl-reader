﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class MissingValuesTests
    {
        [Fact]
        public void MissingValues_UseCase1()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("missing values SEXRACE (0).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<SetMissingValues>(commandList.Commands[0]);
            var command = commandList.Commands[0] as SetMissingValues;

            Assert.Single(command.Variables);
            Assert.Equal("SEXRACE", command.Variables[0]);

            Assert.Equal("0", command.Value[0]);
        }

        [Fact]
        public void MissingValues_UseCase2()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("missing values V520041 (8 thru hi) V520042 V520043 (8 thru hi, 0).");
            int count = commandList.Commands.Count;
            Assert.Equal(2, count);

            Assert.IsType<SetMissingValues>(commandList.Commands[0]);
            var command1 = commandList.Commands[0] as SetMissingValues;
            var command2 = commandList.Commands[1] as SetMissingValues;

            Assert.Equal("V520041", command1.Variables[0]);
            Assert.Equal("8", command1.ValueRange[0].First);
            Assert.Equal("hi", command1.ValueRange[0].Last);

            Assert.Equal("V520042", command2.Variables[0]);
            Assert.Equal("V520043", command2.Variables[1]);
            Assert.Equal("8", command2.ValueRange[0].First);
            Assert.Equal("hi", command2.ValueRange[0].Last);
            Assert.Equal("0", command2.Value[0]);

        }
    }
}
