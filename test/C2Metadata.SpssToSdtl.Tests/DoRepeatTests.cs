﻿using C2Metadata.Common.SpssConverter;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class DoRepeatTests
    {

        [Fact]
        public void DoRepeat_UseCase1()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"get file='da07213_useForDo_Repeat.sav'.
compute Info = 0.
do repeat a = V520173 to V520176.
if ((a ge 1) and(a le 3)) Info = Info + 1.
end repeat.
var label Info 'Number of Information Sources'.
save outfile = 'da07213_DoRepeat_loop.sav'.
execute.
");
            int count = commandList.Commands.Count;
            Assert.Equal(6, count);

            //Assert.IsType<VariableLabelCommand>(commandList.Commands[0]);
            //var command = commandList.Commands[0] as VariableLabelCommand;

        }
    }
}
