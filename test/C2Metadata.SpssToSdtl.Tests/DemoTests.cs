﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.Common.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using sdtl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class DemoTests
    {
        [Fact]
        public void DemoCps()
        {
            string fileName = Path.Combine("..", "..", "..", "..", "data", "cps-demo.sps");
            var converter = new BasicConverter();
            var commandList = converter.ConvertFile(fileName);
            SaveJson(commandList, @"d:\out\cps-demo.sdtl.json");
        }

        [Fact]
        public void DemoExpression()
        {
            string fileName = Path.Combine("..", "..", "..", "..", "data", "expression-demo.sps");
            var converter = new BasicConverter();
            var commandList = converter.ConvertFile(fileName);
            SaveJson(commandList, @"d:\out\expression-demo.sdtl.json");
        }

        [Fact]
        public void OrnulfTest()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("compute BMI=sin((Weight*703)/(Height**2)).");
            SaveJson(commandList, @"d:\out\ornulf-demo.sdtl.json");
        }

        [Fact]
        public void George1_RecodeValueLabels()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"* Encoding: UTF-8.
*recode existing (categorical) variable into new variable.
*assign value labels to new variable.
*rename existing (old) variable.

get file='da07213_inputForRecode'.
recode V520131 (0=0) (1,2=1) (3 thru 6=2) (7,8=3) into EDUC2.
value labels EDUC2 1 'Grade School' 2 'High School' 3 'College' 0 'None'.
rename variables (V520131=EDUC1).
save outfile='da07213_Recode_ValueLabels'.
EXECUTE.

");
            SaveJson(commandList, @"d:\out\recodeValueLabels.sdtl.json");
        }

        [Fact]
        public void George2_RecodeValueLabels2()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString(@"*recode existing (categorical) variable into new variable.
*assign value labels to new variable.
*rename existing (old) variable.

get file='da07213_inputForRecode'.
recode V520131 (0=0) (1,2=1) (3 thru 6=2) (7,8=3) into EDUC2.
value labels EDUC2 1 'Grade School' 2 'High School' 3 'College' 0 'None'.
rename variables (V520131=EDUC1).
save outfile='da07213_Recode_ValueLabels'.
EXECUTE.

");
            SaveJson(commandList, @"d:\out\recodeValueLabels2.sdtl.json");
        }

        private void SaveJson(Program program, string fileName)
        {
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                string json = SdtlSerializer.SerializeAsJson(program);
                json = json.Replace("\r\n", "\n");
                File.WriteAllText(fileName, json);
            }
        }
    }
}
