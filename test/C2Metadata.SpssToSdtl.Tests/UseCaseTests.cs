﻿using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Tests.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class UseCaseTests
    {
        private ITestOutputHelper output;

        public static IEnumerable<object[]> Paths
        {
            get
            {
                var paths = FileSystemHelpers.GatherSpssCommandFiles(@"c:/svn/use_cases");

                foreach (string path in paths)
                {
                    yield return new object[] { path };
                }
            }
        }

        public UseCaseTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Theory]
        [MemberData(nameof(Paths))]
        public void RunConverterOnUseCaseExample(string fileName)
        {
            this.output.WriteLine("Testing " + fileName);

            var converter = new BasicConverter();
            var program = converter.ConvertFile(fileName, true);
            Assert.NotNull(program);

            int lines = program.Commands.Count;
            this.output.WriteLine($"{lines} lines");
        }
    }
}
