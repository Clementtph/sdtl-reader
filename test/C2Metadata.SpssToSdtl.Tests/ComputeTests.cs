﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class ComputeTests
    {
        [Fact]
        public void ComputeConstantNumeric()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 10.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<IntConstantExpression>(compute.Expression);

            var expr = compute.Expression as IntConstantExpression;
            Assert.Equal(10, expr.IntValue);
        }

        [Fact]
        public void ComputeConstantString()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = \"Hello, world.\".");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<StringConstantExpression>(compute.Expression);

            var expr = compute.Expression as StringConstantExpression;
            Assert.Equal("Hello, world.", expr.Value);
        }

        [Fact]
        public void ComputeConstantWithParens()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = (10).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<GroupedExpression>(compute.Expression);

            var expr = compute.Expression as GroupedExpression;

            Assert.IsType<IntConstantExpression>(expr.Expression);
            var intExpr = expr.Expression as IntConstantExpression;
            Assert.Equal(10, intExpr.IntValue);
        }

        [Fact]
        public void ComputeArithmeticAdd()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 + 3.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<AdditionExpression>(compute.Expression);

            var expr = compute.Expression as AdditionExpression;

            Assert.IsType<IntConstantExpression>(expr.Arguments[0]);
            Assert.IsType<IntConstantExpression>(expr.Arguments[1]);

            var term1 = expr.Arguments[0] as IntConstantExpression;
            var term2 = expr.Arguments[1] as IntConstantExpression;

            Assert.Equal(2, term1.IntValue);
            Assert.Equal(3, term2.IntValue);
        }

        [Fact]
        public void ComputeArithmeticSubtract()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 - 3.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<SubtractionExpression>(compute.Expression);

            var expr = compute.Expression as SubtractionExpression;

            Assert.IsType<IntConstantExpression>(expr.Arguments[0]);
            Assert.IsType<IntConstantExpression>(expr.Arguments[1]);

            var term1 = expr.Arguments[0] as IntConstantExpression;
            var term2 = expr.Arguments[1] as IntConstantExpression;

            Assert.Equal(2, term1.IntValue);
            Assert.Equal(3, term2.IntValue);
        }

        [Fact]
        public void ComputeArithmeticMultiply()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 * 3.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<MultiplicationExpression>(compute.Expression);

            var expr = compute.Expression as MultiplicationExpression;

            Assert.IsType<IntConstantExpression>(expr.Arguments[0]);
            Assert.IsType<IntConstantExpression>(expr.Arguments[1]);

            var term1 = expr.Arguments[0] as IntConstantExpression;
            var term2 = expr.Arguments[1] as IntConstantExpression;

            Assert.Equal(2, term1.IntValue);
            Assert.Equal(3, term2.IntValue);
        }

        [Fact]
        public void ComputeArithmeticDivide()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 10 / 2.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<DivisionExpression>(compute.Expression);

            var expr = compute.Expression as DivisionExpression;

            Assert.IsType<IntConstantExpression>(expr.Arguments[0]);
            Assert.IsType<IntConstantExpression>(expr.Arguments[1]);

            var term1 = expr.Arguments[0] as IntConstantExpression;
            var term2 = expr.Arguments[1] as IntConstantExpression;

            Assert.Equal(10, term1.IntValue);
            Assert.Equal(2, term2.IntValue);
        }

        [Fact]
        public void ComputeArithmeticExponent()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = 2 ** 16.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);
            
            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<ExponentExpression>(compute.Expression);

            var expr = compute.Expression as ExponentExpression;

            Assert.IsType<IntConstantExpression>(expr.Arguments[0]);
            Assert.IsType<IntConstantExpression>(expr.Arguments[1]);

            var term1 = expr.Arguments[0] as IntConstantExpression;
            var term2 = expr.Arguments[1] as IntConstantExpression;

            Assert.Equal(2, term1.IntValue);
            Assert.Equal(16, term2.IntValue);
        }

        [Fact]
        public void ComputeArithmeticComplex1()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = (2 + 4) - 10 * 2**3.");
            Assert.Single(commandList.Commands);

            // The correct tree here is:
            // SUBTRACT
            //   GROUP
            //     ADD
            //       2
            //       4
            //   MULTIPLY
            //     10
            //     EXPONENT
            //       2
            //       3

            // Compute command with var1 as the variable.
            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;
            Assert.Equal("var1", compute.Variable);

            // Subtraction of the Group minus all the other stuff
            Assert.IsType<SubtractionExpression>(compute.Expression);
            var subtraction = compute.Expression as SubtractionExpression;

            // Subtraction left side: Grouped Addition of 2 + 4
            Assert.IsType<GroupedExpression>(subtraction.Arguments[0]);
            var group = subtraction.Arguments[0] as GroupedExpression;
            Assert.IsType<AdditionExpression>(group.Expression);
            var addition = group.Expression as AdditionExpression;

            // 2
            Assert.IsType<IntConstantExpression>(addition.Arguments[0]);
            var intLit1 = addition.Arguments[0] as IntConstantExpression;
            Assert.Equal(2, intLit1.IntValue);

            // 4
            Assert.IsType<IntConstantExpression>(addition.Arguments[1]);
            var intLit2 = addition.Arguments[1] as IntConstantExpression;
            Assert.Equal(4, intLit2.IntValue);

            // Subtraction right side: multiplication of 10 * 2**8
            Assert.IsType<MultiplicationExpression>(subtraction.Arguments[1]);
            var multiplication = subtraction.Arguments[1] as MultiplicationExpression;
            
            // Multiplication left side: 10
            Assert.IsType<IntConstantExpression>(multiplication.Arguments[0]);
            var intLit3 = multiplication.Arguments[0] as IntConstantExpression;
            Assert.Equal(10, intLit3.IntValue);

            // Multiplication right side: 2**3
            Assert.IsType<ExponentExpression>(multiplication.Arguments[1]);
            var exponent = multiplication.Arguments[1] as ExponentExpression;

            // 2
            Assert.IsType<IntConstantExpression>(exponent.Arguments[0]);
            var intLit4 = exponent.Arguments[0] as IntConstantExpression;
            Assert.Equal(2, intLit4.IntValue);

            // 3
            Assert.IsType<IntConstantExpression>(exponent.Arguments[1]);
            var intLit5 = exponent.Arguments[1] as IntConstantExpression;
            Assert.Equal(3, intLit5.IntValue);
        }

        [Fact]
        public void ComputeArithmeticComplex2()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = (2 + var2) - 10 * 2**var3.");
            Assert.Single(commandList.Commands);

            // Compute command with var1 as the variable.
            Assert.IsType<Compute>(commandList.Commands[0]);
            var compute = commandList.Commands[0] as Compute;
            Assert.Equal("var1", compute.Variable);

            // Subtraction of the Group minus all the other stuff
            Assert.IsType<SubtractionExpression>(compute.Expression);
            var subtraction = compute.Expression as SubtractionExpression;

            // Subtraction left side: Grouped Addition of 2 + var2
            Assert.IsType<GroupedExpression>(subtraction.Arguments[0]);
            var group = subtraction.Arguments[0] as GroupedExpression;
            Assert.IsType<AdditionExpression>(group.Expression);
            var addition = group.Expression as AdditionExpression;

            // 2
            Assert.IsType<IntConstantExpression>(addition.Arguments[0]);
            var intLit1 = addition.Arguments[0] as IntConstantExpression;
            Assert.Equal(2, intLit1.IntValue);

            // var2
            Assert.IsType<VariableSymbolExpression>(addition.Arguments[1]);
            var symbol1 = addition.Arguments[1] as VariableSymbolExpression;
            Assert.Equal("var2", symbol1.VariableName);

            // Subtraction right side: multiplication of 10 * 2**var3
            Assert.IsType<MultiplicationExpression>(subtraction.Arguments[1]);
            var multiplication = subtraction.Arguments[1] as MultiplicationExpression;
            
            // Multiplication left side: 10
            Assert.IsType<IntConstantExpression>(multiplication.Arguments[0]);
            var intLit3 = multiplication.Arguments[0] as IntConstantExpression;
            Assert.Equal(10, intLit3.IntValue);

            // Multiplication right side: 2**var3
            Assert.IsType<ExponentExpression>(multiplication.Arguments[1]);
            var exponent = multiplication.Arguments[1] as ExponentExpression;

            // 2
            Assert.IsType<IntConstantExpression>(exponent.Arguments[0]);
            var intLit4 = exponent.Arguments[0] as IntConstantExpression;
            Assert.Equal(2, intLit4.IntValue);

            // var3
            Assert.IsType<VariableSymbolExpression>(exponent.Arguments[1]);
            var symbol2 = exponent.Arguments[1] as VariableSymbolExpression;
            Assert.Equal("var3", symbol2.VariableName);
        }

        [Fact]
        public void ComputeCopy()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var1 = var2.");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;

            Assert.Equal("var1", compute.Variable);
            Assert.IsType<VariableSymbolExpression>(compute.Expression);

            var expr = compute.Expression as VariableSymbolExpression;

            Assert.Equal("var2", expr.VariableName);
        }

        [Fact]
        public void ComputeFunctionSingleParameter()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE sqrt = SQRT(var4).");
            Assert.Single(commandList.Commands);

            var compute = commandList.Commands[0] as Compute;
            Assert.Equal("sqrt", compute.Variable);
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.Equal("SQRT", expr.Name);
            Assert.Single(expr.Arguments);

            var param = expr.Arguments[0];

            Assert.IsType<VariableSymbolExpression>(param);
            var paramExpr = param as VariableSymbolExpression;
            Assert.Equal("var4", paramExpr.VariableName);
        }

        // COMPUTE C1PIDATE=date.dmy(number(substr(ltrim(date),3,2),f2.0), 
    //number(substr(ltrim(date),1,2),f2.0), number(substr(ltrim(date),5),f4.0)).


        [Fact]
        public void ComputeFunctionMultipleParameters()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE avg = MEAN(var1, var2, var3, var4).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            var compute = commandList.Commands[0] as Compute;
            Assert.Equal("avg", compute.Variable);
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            var expr = compute.Expression as FunctionCallExpression;

            Assert.Equal("MEAN", expr.Name);
            Assert.Equal(4, expr.Arguments.Count);

            var param1 = expr.Arguments[0] as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(param1);
            var param1Expr = param1 as VariableSymbolExpression;
            Assert.Equal("var1", param1Expr.VariableName);

            var param2 = expr.Arguments[1] as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(param2);
            var param2Expr = param2 as VariableSymbolExpression;
            Assert.Equal("var2", param2Expr.VariableName);
        }

        [Fact]
        public void ComputedNestedFunctions()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE sqrt = TRUNC(SQRT(var4)).");
            int count = commandList.Commands.Count;
            Assert.Single(commandList.Commands);

            var compute = commandList.Commands[0] as Compute;
            Assert.Equal("sqrt", compute.Variable);
            Assert.IsType<FunctionCallExpression>(compute.Expression);

            // TRUNC function call
            var truncExpr = compute.Expression as FunctionCallExpression;
            Assert.Equal("TRUNC", truncExpr.Name);
            Assert.Single(truncExpr.Arguments);

            // TRUNC's parameter is the SQRT function call
            var param = truncExpr.Arguments[0] as ExpressionBase;
            Assert.IsType<FunctionCallExpression>(param);
            var sqrtExpr = param as FunctionCallExpression;
            Assert.Equal("SQRT", sqrtExpr.Name);

            // SQRT's parameter is the symbol var4.
            Assert.Single(sqrtExpr.Arguments);
            var sqrtParam = sqrtExpr.Arguments[0] as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(sqrtParam);
            var sqrtSymbolExpression = sqrtParam as VariableSymbolExpression;
            Assert.Equal("var4", sqrtSymbolExpression.VariableName);
        }

        [Fact]
        public void ComputeSumN()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var5 = SUM.2(var1, var2, var3).");
            int count = commandList.Commands.Count;
            Assert.Single(commandList.Commands);

            var compute = commandList.Commands[0] as Compute;
            Assert.Equal("var5", compute.Variable);

            Assert.IsType<FunctionCallExpression>(compute.Expression);
            var expr = compute.Expression as FunctionCallExpression;
            Assert.Equal("SUM.2", expr.Name);

            Assert.Equal(3, expr.Arguments.Count);

            var param = expr.Arguments[0] as ExpressionBase;
            Assert.IsType<VariableSymbolExpression>(param);
            var paramExpr = param as VariableSymbolExpression;
            Assert.Equal("var1", paramExpr.VariableName);
        }

        [Fact]
        public void FunctionCallWithVariableRange()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("COMPUTE var5 = MEAN(var1 TO var3).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);
        }
    }
}
