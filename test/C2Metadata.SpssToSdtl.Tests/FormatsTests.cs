﻿using C2Metadata.Common.SpssConverter;
using sdtl;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class FormatsTests
    {
        [Fact]
        public void PrintFormat_UseCase1()
        {
            var converter = new BasicConverter();
            var commandList = converter.ConvertString("print formats Logpop (f3.2).");
            int count = commandList.Commands.Count;
            Assert.Equal(1, count);

            Assert.IsType<SetDisplayFormat>(commandList.Commands[0]);
            var command = commandList.Commands[0] as SetDisplayFormat;

            Assert.Single(command.Variables);
            Assert.Equal("Logpop", command.Variables[0]);
            Assert.Equal("f3.2", command.Format);
        }
    }
}
