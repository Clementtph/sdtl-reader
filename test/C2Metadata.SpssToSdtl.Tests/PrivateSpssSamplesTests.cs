using C2Metadata.Common.SpssConverter;
using C2Metadata.SpssToSdtl.Cli;
using C2Metadata.SpssToSdtl.Tests.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;
using Xunit.Abstractions;
using Xunit.Extensions;

namespace C2Metadata.SpssToSdtl.Tests
{
    public class PrivateSpssSamplesTests
    {
        private ITestOutputHelper output;

        public static IEnumerable<object[]> Paths
        {
            get
            {
                var paths = FileSystemHelpers.GatherSpssCommandFiles(@"c:/svn/spss-samples-private");

                foreach (string path in paths)
                {
                    yield return new object[] { path };
                }
            }
        }

        public PrivateSpssSamplesTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Theory]
        [MemberData(nameof(Paths))]
        public void RunConverterOnPrivateSpssSample(string fileName)
        {
            this.output.WriteLine("Testing " + fileName);

            var converter = new BasicConverter();
            var program = converter.ConvertFile(fileName, true);
            int lines = program.Commands.Count;

            Assert.Empty(program.Messages);

            this.output.WriteLine($"{lines} lines");
        }

    }
}