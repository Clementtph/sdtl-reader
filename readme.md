# SDTL Tools

## SDTL Reader Desktop Application

| Platform   | Status                                                                                                                                              |
| ---------- | --------                                                                                                                                            |
| Windows    | [![Build status](https://ci.appveyor.com/api/projects/status/ipyb382r3i5dusx4?svg=true)](https://ci.appveyor.com/project/JeremyIverson/sdtl-reader) |
| macOS      | Forthcoming                                                                                                                                         |
| Linux      | Forthcoming                                                                                                                                         |

## SPSS to SDTL Converter

Extract and understand transforms from SPSS command files (.sps), and represent 
them using a Structured Data Transform Language (SDTL).

Current support includes:

* Get
* Save
* Title
* Subtitle
* Recode
* Rename Variables
* Execute

Up next:

* Unconditional Assignments
* Conditional assignments
* Delete Variables
* Formats
* Compute
* Add Value Labels
* Variable Labels
* Conditional Recodes

Needs improvement:

* Recode handling (for example, conditional recodes)
* Save: drop, keep, and rename variables

## Web Application

A docker image containing a basic web application can be downloaded from 
https://gitlab.com/c2metadata/sdtl-reader/container_registry

Example container installation:

    docker pull registry.gitlab.com/c2metadata/sdtl-reader:latest

    docker run -d -p 8080:80 registry.gitlab.com/c2metadata/sdtl-reader
    
Example usage:

    curl -X POST "http://localhost:8080/api/spsstosdtl" -d "COMPUTE x = 1"

## Example JSON Instances

Some example JSON instances are created by the SPSS to SDTL tools every time
an update is made to those tools.

* [CPS transforms example](http://ci.appveyor.com/api/projects/JeremyIverson/sdtl-reader/artifacts/src/C2Metadata.SpssToSdtl.Cli/cps-demo.sdtl.json)
* [Expression example](http://ci.appveyor.com/api/projects/JeremyIverson/sdtl-reader/artifacts/src/C2Metadata.SpssToSdtl.Cli/expression-demo.sdtl.json)
